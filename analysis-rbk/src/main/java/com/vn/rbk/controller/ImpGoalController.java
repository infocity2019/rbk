/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vn.rbk.controller;

import com.vn.rbk.AppConfig;
import com.vn.rbk.domain.goalnow;
import com.vn.rbk.services.base.GoalServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/goal")
public class ImpGoalController {
    @Autowired
    private GoalServices goalServices;

    @Autowired
    private AppConfig myConfig;

    @CrossOrigin
    @GetMapping(value = "/now")
    public ResponseEntity<?> goalsnow() {
        String URL = "https://www.goalsnow.com/over-under-predictions/";
        goalServices.alsGoalNow(URL);
        return new ResponseEntity<>("Done", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/olbg")
    public ResponseEntity<?> impOlbg() {
        String URL = "https://www.olbg.com/betting-tips/Football/1";
        goalServices.alsOLBG(URL);
        return new ResponseEntity<>("Done", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/list")
    public ResponseEntity<?> goalList(
            @RequestParam(value = "date", defaultValue = "") String inpdate,
            @RequestParam(value = "name", defaultValue = "") String name
    ) {
        List<goalnow> goalnowList = goalServices.showGoalNow(inpdate, name);
        return new ResponseEntity<>(goalnowList, HttpStatus.OK);
    }
}
