package com.vn.rbk.controller;

import com.vn.rbk.services.base.PushServices;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@Controller
@RequestMapping("/push")
public class PushController {
    public static final Logger logger = LoggerFactory.getLogger(PushController.class);
    @Autowired
    private PushServices pushServices;

    @CrossOrigin
    @PutMapping(value = "/msg")
    public ResponseEntity<String> pushMessage(@RequestParam(value = "text", defaultValue = "") String text) {
        String resMsg = pushServices.pushTele(text);
        return ResponseEntity.status(HttpStatus.OK).body(resMsg);
    }
}
