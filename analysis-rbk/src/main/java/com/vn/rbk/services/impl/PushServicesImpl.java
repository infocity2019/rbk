package com.vn.rbk.services.impl;

import com.vn.rbk.services.base.PushServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Service
@Slf4j
public class PushServicesImpl implements PushServices {
    @Override
    public String pushTele(String inputText) {
        String response = null;
        try {
            String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";

            String apiToken = "1657125444:AAET5ndQRHVZd_P9MOCVDj6m3wHHXJlOV44";
            String chatId = "@mctrading2021";

            urlString = String.format(urlString, apiToken, chatId, inputText);

            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();

            StringBuilder sb = new StringBuilder();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            response = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
}
