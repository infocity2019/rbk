package com.vn.rbk.services.impl;

import com.vn.rbk.domain.goalnow;
import com.vn.rbk.repository.base.GoalRepo;
import com.vn.rbk.services.base.GoalServices;
import com.vn.rbk.util.Validator;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
@Slf4j
public class GoalServicesImpl implements GoalServices {

    @Autowired
    private GoalRepo goalRepo;

    @Override
    public void alsGoalNow(String inputURL) {
        List<goalnow> goalnows = parseGoal(inputURL);
        goalRepo.insertGoalNow(goalnows);
    }

    @Override
    public void alsOLBG(String url) {
        List<goalnow> goalnows = parseOLBG(url);
        goalRepo.insertGoalNow(goalnows);
    }

    private List<goalnow> parseOLBG(String url) {
        List<goalnow> goalnowList = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();
            Elements goals = doc.getElementsByTag("tbody");
            for (Element ele : goals) {
                Elements elementTipRow = ele.getElementsByClass("tip-row");
                if (Validator.validateCollection(elementTipRow)) {
                    for (Element e : elementTipRow) {
                        Elements tdElement = e.getElementsByTag("td");
                        for (Element el : tdElement){
                            Elements match = el.getElementsByTag("td");
                            for (Element element : match) {
//                                element.get
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return goalnowList;
    }

    @Override
    public List<goalnow> showGoalNow(String inpdate, String name) {
        List<goalnow> goalnowList = goalRepo.listGoalNow(inpdate, name);
        return goalnowList;
    }

    private List<goalnow> parseGoal(String inputURL) {
        List<goalnow> goalnowList = new ArrayList<>();
        try {
            String flag = "";
            Document doc = Jsoup.connect(inputURL).get();
            Elements goals = doc.getElementsByTag("tr");
            for (Element element : goals) {
                Elements toptr = element.getElementsByClass("toptr");
                Elements timemob = element.getElementsByClass("timemob");
                Elements flags = element.getElementsByClass("flag");
                if (toptr.size() < 1 && timemob.size() < 1) {
                    if (flags.size() > 0) {
                        flag = element.getElementsByTag("span").html();
                    } else {
                        Elements matchinfo = element.getElementsByTag("td");
                        goalnow gn = new goalnow();
                        boolean isHome = false;
                        boolean isTip = false;
                        for (Element elm : matchinfo) {
                            gn.setTournament(flag);
                            if (elm.getElementsByClass("timetable").size() > 0) {
                                String startTime = elm.getElementsByClass("linkdisplay").html();
                                SimpleDateFormat formDate = new SimpleDateFormat("dd-MM-yyyy");
                                String strDate = formDate.format(new Date());
                                startTime = strDate + " " + startTime;
                                DateTimeFormatter formatter =
                                        DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm", Locale.UK);
                                LocalDateTime localDateTime = LocalDateTime.parse(startTime, formatter);
                                if (Validator.validate(localDateTime)) {
                                    gn.setTime(localDateTime.plusHours(7).toString());
                                }
                            }
                            if (elm.getElementsByClass("nextline").size() > 0 && isHome == false) {
                                gn.setHome(elm.getElementsByClass("matchsep").html());
                                isHome = true;
                            }
                            if (elm.getElementsByClass("nextline awaymob").size() > 0) {
                                gn.setAway(elm.getElementsByClass("matchsep").html());
                            }
                            if (elm.getElementsByClass("center").size() > 0 && isTip == false) {
                                gn.setTip(elm.getElementsByTag("span").html());
                                isTip = true;
                            }
                            if (elm.getElementsByClass("center tipgo").size() > 0) {
                                gn.setResult(elm.getElementsByClass("match-name-result").html());
                            }
                            Date newDate = new Date();
                            gn.setInpDate(new SimpleDateFormat("yyyy-MM-dd").format(newDate));
                            gn.setInpTime(newDate.getTime());
                        }
                        goalnowList.add(gn);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return goalnowList;
    }

}
