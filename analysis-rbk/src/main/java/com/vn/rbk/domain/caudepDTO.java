package com.vn.rbk.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class caudepDTO {
    String limitnhaylon;
    String ngaychot;
    String thongke;
    List<Map<String, String>> caudep = new ArrayList<>();
}
